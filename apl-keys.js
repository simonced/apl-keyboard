document.addEventListener("DOMContentLoaded", aplKeyInit);

// each key definition is like:
// normal, shifted, apl1 apl2
// apl2 is "shifted"
const layouts = {
  ja_jp: [
    [
      "全半",
      "1 ! ¨ ⌶",
      '2 " ¯ ≢',
      "3 # < ⍒",
      "4 $ ≤ ⍋",
      "5 % = ⌽",
      "6 & ≥ ⊖",
      "7 ' > ⍕",
      "8 ( ≠ ⍱",
      "9 ) ∨ ⍲",
      "0   ∧", // in fact ⌺ is aslo available here...
      "- = × ÷",
      "^ ~ ⍉ ⌺",
      "￥ | ⊢ ⊣",
      "←",
    ],
    [
      "↹",
      "q Q ?",
      "w W ⍵",
      "e E ∊ ⍷",
      "r R ⍴",
      "t T ~ ⍨",
      "y Y ↑",
      "u U ↓",
      "i I ⍳ ⍸",
      "o O ○ ⍥",
      "p P * ⍣",
      "@ ` ⍫",
      "[ { ← ⍞",
    ],
    [
      "⇫",
      "a A ⍺",
      "s S ⌈",
      "d D ⌊",
      "f F _",
      "g G ∇",
      "h H ∆",
      "j J ∘ ⍤",
      "k K ' ⌸",
      "l L ⎕ ⌷",
      "; + ⍎ ⌹",
      ": * ≡ ⍟",
      "] } → ⍬",
    ],
    [
      "⇪",
      "z Z ⊂ ⊆",
      "x X ⊃ ",
      "c C ∩ ",
      "v V ∪ ",
      "b B ⊥ ",
      "n N ⊤ ",
      "m M | ",
      ", < ⍝ ⍪",
      ". > ⍀ ⍙",
      "/ ? ⌿ ⍠",
      "\\ _ ⊢ !",
    ],
  ],
}

function aplKeyInit() {
  document.querySelectorAll(".keyboard").forEach(x => {
    aplKeyInitLayout(x, x.getAttribute("layout"));
  });
}

/**
 * setup keys for a keyboard in a specific layout
 * @param {HTMLElement} keyb 
 * @param {string} layout 
 */
function aplKeyInitLayout(keyb, layout) {
  if(!layouts[layout]) {
    keyb.innerText = "Undefined layout: "+layout;
    return;
  }

  let keydef, row, key, symbol;
  for(r in layouts[layout]) {
    // new row
    row = document.createElement("div");
    row.classList.add("row");
    for(k in layouts[layout][r]) {
      keydef = layouts[layout][r][k].split(" ");
      // prepare new key
      key = document.createElement("div");
      key.classList.add("key");
      // key symbols
      for(let s=0; s<4; ++s) {
        if(!keydef[s]) continue;
        symbol = document.createElement("span");
        symbol.classList.add("symb"+s);
        symbol.innerText = keydef[s];
        key.append(symbol);
      }

      // add key to the row
      row.append(key);
    }
    // add row to the keyb
    keyb.append(row);
  }
}